package com.bp.business;

import com.bp.dto.objetos.login.LoginRequest;
import com.bp.dto.objetos.login.LoginResponse;
import com.bp.dto.utils.RequestAPIDTO;
import com.bp.dto.utils.RequestAPITipoEnum;
import com.bp.dto.utils.ResponseAPIDTO;
import com.bp.util.ConsumeAPIUtil;
import com.bp.util.ObjetoChaveValor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class LoginBusiness {
    public LoginResponse login(String urlBase, long agencia, long conta) {
        System.out.println("[ EXECUCAO ] - LOGIN - INICIO");

        try {
            ConsumeAPIUtil consumeAPIUtil = new ConsumeAPIUtil();
            Gson gson = new GsonBuilder().create();

            LoginRequest loginRequest = new LoginRequest();
            loginRequest.setAgencia(agencia);
            loginRequest.setConta(conta);
            loginRequest.setSenha("88888888");
            loginRequest.setTitularidade(1);

            String jsonRequest = gson.toJson(loginRequest);

            List<ObjetoChaveValor<String, String>> listaHeaders = new ArrayList<>();
            listaHeaders.add(new ObjetoChaveValor<>("User-Agent", "motorola;XT1563;Android;6.0.1;MPD24.107-52;mov-android-app;6.33.0.2;pt_BR;cpu=0|clock=1651200|ram=1960012 kB|espacoSDInterno=25,45 GB|espacoSDExterno=25,45 GB|isSmartphone=true|nfc=true|camera=true|cameraFrontal=true|root=false|reconhecimentoVoz=true|resolucao=1080_1776|densidade=3.0|imei=123456789012345|"));
            listaHeaders.add(new ObjetoChaveValor<>("X-CFE-Deviceinfo", "eyJpZGgiOiIzMTcwZWJjZWZkOGU5ZmI2MGRkZjdjZTUzY2QyZmMwYmY2OTJiODJiYjQwMjI3ZTZmOThmNjcyZDhlYTAzMTVmYTU2ZmY2MmNmNjM2YzZhNSIsImFwZWxpZG8iOiJmYWJyaWNpb3BlZHJlaXJhIiwibGF0aXR1ZGUiOiIiLCJsb25naXR1ZGUiOiIiLCJpZERpc3Bvc2l0aXZvIjoiMjdlZGVjYTJiMGIyNWViZDg0ZmUyZmIzNDRkNjlhZmQifQ=="));
            listaHeaders.add(new ObjetoChaveValor<>("X-CNL-Ticket", "002000091PNVKlF2LwM2BSBF00000000"));

            RequestAPIDTO requestAPIDTO = new RequestAPIDTO();
            requestAPIDTO.setTipo(RequestAPITipoEnum.POST);
            requestAPIDTO.setUrl(urlBase + "/cfe-acesso/api/v2/token/MOV_PESSOA_FISICA_AGENCIA_CONTA/login");
            requestAPIDTO.setCabecalhos(listaHeaders);
            requestAPIDTO.setObjetoJson(jsonRequest);

            ResponseAPIDTO responseAPIDTO = consumeAPIUtil.executa(requestAPIDTO);

            if(responseAPIDTO != null) {
                if(responseAPIDTO.getStatus().is2xxSuccessful()) {
                    LoginResponse loginResponse = gson.fromJson(responseAPIDTO.getObjetoJson(), LoginResponse.class);

                    System.out.println();
                    System.out.println("[ EXECUCAO ] - LOGIN - CONCLUIDO COM SUCESSO");

                    return loginResponse;
                } else {
                    System.out.println("[ EXECUCAO ] - LOGIN - ERRO STATUS: " + responseAPIDTO.getStatus());
                }
            } else {
                System.out.println("[ EXECUCAO ] - LOGIN - ERRO RESPONSE NULL");
            }
        } catch (Exception e) {
            System.out.println("[ EXECUCAO ] - LOGIN - EXCEPTION");
            System.out.println();
            e.printStackTrace();
            System.out.println();
        }

        return null;
    }
}
