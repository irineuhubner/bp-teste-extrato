package com.bp.business;

import com.bp.dto.objetos.saldo.SaldoResponse;
import com.bp.dto.utils.ExecucaoItemDTO;
import com.bp.dto.utils.RequestAPIDTO;
import com.bp.dto.utils.RequestAPITipoEnum;
import com.bp.dto.utils.ResponseAPIDTO;
import com.bp.util.AppUtil;
import com.bp.util.ConsumeAPIUtil;
import com.bp.util.ObjetoChaveValor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.List;

public class SaldoBusiness {
    private String urlBase;
    private String token;
    private String cpf;
    private long mci;

    public SaldoBusiness(String urlBase, String token, String cpf, long mci) {
        this.urlBase = urlBase;
        this.token = token;
        this.cpf = cpf;
        this.mci = mci;
    }

    public ExecucaoItemDTO buscar() {
        ExecucaoItemDTO itemDTO = new ExecucaoItemDTO();

        try {
            ConsumeAPIUtil consumeAPIUtil = new ConsumeAPIUtil();
            Gson gson = new GsonBuilder().create();

            List<ObjetoChaveValor<String, String>> listaHeaders = new ArrayList<>();
            listaHeaders.add(new ObjetoChaveValor<>("Authorization", "Bearer " + token));

            RequestAPIDTO requestAPIDTO = new RequestAPIDTO();
            requestAPIDTO.setTipo(RequestAPITipoEnum.GET);
            requestAPIDTO.setUrl(urlBase + "/cfe-bpr/api/v1/extrato/consolidado/6/11/" + mci + "/" + cpf);
            requestAPIDTO.setCabecalhos(listaHeaders);

            itemDTO.setDataInicio(AppUtil.datetimeNowMiliSecondsStringENG());

            ResponseAPIDTO responseAPIDTO = consumeAPIUtil.executa(requestAPIDTO);

            itemDTO.setDataFim(AppUtil.datetimeNowMiliSecondsStringENG());
            itemDTO.setTempo(AppUtil.tempoExecucao(itemDTO.getDataInicio(), itemDTO.getDataFim()));

            if(responseAPIDTO != null) {
                if(responseAPIDTO.getStatus().is2xxSuccessful()) {
                    SaldoResponse saldoResponse = gson.fromJson(responseAPIDTO.getObjetoJson(), SaldoResponse.class);

                    if(saldoResponse != null && saldoResponse.getResponse() != null && (saldoResponse.getResponse().getSaldoInvestimento().doubleValue() > 0) || saldoResponse.getResponse().getSaldoRendimentoInvestimento().doubleValue() > 0) {
                        itemDTO.setSucesso();
                    } else {
                        itemDTO.setErro();
                    }
                } else {
                    itemDTO.setErro();
                }
            } else {
                itemDTO.setErro();
            }
        } catch (Exception e) {
            itemDTO.setErro();
        }

        return itemDTO;
    }
}
