package com.bp.business;

import com.bp.dto.objetos.extrato.ExtratoResponse;
import com.bp.dto.utils.ExecucaoItemDTO;
import com.bp.dto.utils.RequestAPIDTO;
import com.bp.dto.utils.RequestAPITipoEnum;
import com.bp.dto.utils.ResponseAPIDTO;
import com.bp.util.AppUtil;
import com.bp.util.ConsumeAPIUtil;
import com.bp.util.ObjetoChaveValor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.List;

public class ExtratoBusiness {
    private String urlBase;
    private String token;
    private String cpf;
    private long mci;

    public ExtratoBusiness(String urlBase, String token, String cpf, long mci) {
        this.urlBase = urlBase;
        this.token = token;
        this.cpf = cpf;
        this.mci = mci;
    }

    public ExecucaoItemDTO buscar() {
        ExecucaoItemDTO itemDTO = new ExecucaoItemDTO();

        try {
            ConsumeAPIUtil consumeAPIUtil = new ConsumeAPIUtil();
            Gson gson = new GsonBuilder().create();

            List<ObjetoChaveValor<String, String>> listaHeaders = new ArrayList<>();
            listaHeaders.add(new ObjetoChaveValor<>("Authorization", "Bearer " + token));

            RequestAPIDTO requestAPIDTO = new RequestAPIDTO();
            requestAPIDTO.setTipo(RequestAPITipoEnum.GET);
            requestAPIDTO.setUrl(urlBase + "/cfe-bpr/api/v1/extrato/6/11/" + mci + "/" + cpf);
            requestAPIDTO.setCabecalhos(listaHeaders);

            itemDTO.setDataInicio(AppUtil.datetimeNowMiliSecondsStringENG());

            ResponseAPIDTO responseAPIDTO = consumeAPIUtil.executa(requestAPIDTO);

            itemDTO.setDataFim(AppUtil.datetimeNowMiliSecondsStringENG());
            itemDTO.setTempo(AppUtil.tempoExecucao(itemDTO.getDataInicio(), itemDTO.getDataFim()));

            if(responseAPIDTO != null) {
                if(responseAPIDTO.getStatus().is2xxSuccessful()) {
                    ExtratoResponse extratoResponse = gson.fromJson(responseAPIDTO.getObjetoJson(), ExtratoResponse.class);

                    if(extratoResponse != null && extratoResponse.getResponse() != null && extratoResponse.getResponse().getCdStatusHttp().equals("200")) {
                        itemDTO.setSucesso();
                    } else {
                        itemDTO.setErro();
                    }
                } else {
                    itemDTO.setErro();
                }
            } else {
                itemDTO.setErro();
            }
        } catch (Exception e) {
            itemDTO.setErro();
        }

        return itemDTO;
    }
}
