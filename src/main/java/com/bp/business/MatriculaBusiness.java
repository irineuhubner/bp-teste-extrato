package com.bp.business;

import com.bp.dto.objetos.matricula.MatriculaResponse;
import com.bp.dto.utils.ExecucaoItemDTO;
import com.bp.dto.utils.RequestAPIDTO;
import com.bp.dto.utils.RequestAPITipoEnum;
import com.bp.dto.utils.ResponseAPIDTO;
import com.bp.util.AppUtil;
import com.bp.util.ConsumeAPIUtil;
import com.bp.util.ObjetoChaveValor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

public class MatriculaBusiness {
    private String urlBase;
    private String token;
    private String cpf;
    private long mci;

    public MatriculaBusiness(String urlBase, String token, String cpf, long mci) {
        this.urlBase = urlBase;
        this.token = token;
        this.cpf = cpf;
        this.mci = mci;
    }

    public ExecucaoItemDTO buscar() {
        ExecucaoItemDTO itemDTO = new ExecucaoItemDTO();

        try {
            ConsumeAPIUtil consumeAPIUtil = new ConsumeAPIUtil();
            Gson gson = new GsonBuilder().create();

            List<ObjetoChaveValor<String, String>> listaHeaders = new ArrayList<>();
            listaHeaders.add(new ObjetoChaveValor<>("Authorization", "Bearer " + token));

            RequestAPIDTO requestAPIDTO = new RequestAPIDTO();
            requestAPIDTO.setTipo(RequestAPITipoEnum.GET);
            requestAPIDTO.setUrl(urlBase + "/cfe-bpr/api/v1/matricula/6/11/" + mci + "/" + cpf);
            requestAPIDTO.setCabecalhos(listaHeaders);

            itemDTO.setDataInicio(AppUtil.datetimeNowMiliSecondsStringENG());

            ResponseAPIDTO responseAPIDTO = consumeAPIUtil.executa(requestAPIDTO);

            itemDTO.setDataFim(AppUtil.datetimeNowMiliSecondsStringENG());
            itemDTO.setTempo(AppUtil.tempoExecucao(itemDTO.getDataInicio(), itemDTO.getDataFim()));

            if(responseAPIDTO != null) {
                if(responseAPIDTO.getStatus().is2xxSuccessful()) {
                    MatriculaResponse matriculaResponse = gson.fromJson(responseAPIDTO.getObjetoJson(), MatriculaResponse.class);

                    if(matriculaResponse != null && matriculaResponse.getResponse() != null && matriculaResponse.getResponse().getCdStatusHttp().equals("200")) {
                        itemDTO.setSucesso();
                    } else {
                        itemDTO.setErro();
                    }
                } else {
                    itemDTO.setErro();
                }
            } else {
                itemDTO.setErro();
            }
        } catch (Exception e) {
            itemDTO.setErro();
        }

        return itemDTO;
    }

    public MatriculaResponse buscarLista() {
        try {
            ConsumeAPIUtil consumeAPIUtil = new ConsumeAPIUtil();
            Gson gson = new GsonBuilder().create();

            List<ObjetoChaveValor<String, String>> listaHeaders = new ArrayList<>();
            listaHeaders.add(new ObjetoChaveValor<>("Authorization", "Bearer " + token));

            RequestAPIDTO requestAPIDTO = new RequestAPIDTO();
            requestAPIDTO.setTipo(RequestAPITipoEnum.GET);
            requestAPIDTO.setUrl(urlBase + "/cfe-bpr/api/v1/matricula/6/11/" + mci + "/" + cpf);
            requestAPIDTO.setCabecalhos(listaHeaders);

            ResponseAPIDTO responseAPIDTO = consumeAPIUtil.executa(requestAPIDTO);

            if(responseAPIDTO != null) {
                if(responseAPIDTO.getStatus().is2xxSuccessful()) {
                    MatriculaResponse matriculaResponse = gson.fromJson(responseAPIDTO.getObjetoJson(), MatriculaResponse.class);

                    if(matriculaResponse != null && matriculaResponse.getResponse() != null && matriculaResponse.getResponse().getCdStatusHttp().equals("200")) {
                        return matriculaResponse;
                    }
                }
            }
        } catch (Exception e) {
            System.out.println();
            e.printStackTrace();
            System.out.println();
        }

        return null;
    }
}
