package com.bp.business;

import com.bp.dto.objetos.login.LoginResponse;
import com.bp.dto.objetos.matricula.MatriculaResponse;
import com.bp.dto.objetos.mci.MciCpfResponse;
import com.bp.dto.utils.ExecucaoItemDTO;
import com.bp.dto.utils.ExecucaoRequisitaDTO;
import com.bp.dto.utils.TipoExecucaoEnum;
import com.bp.util.AppUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicLong;

public class RunBusiness {
    public void executa(String urlBase, long agencia, long conta, long qtd, String email, boolean execMatriculas, boolean execExtrato, boolean execSaldo, boolean execProjRenda, boolean execProjReserva) {
        linha();
        System.out.println("[ EXECUCAO ] - INICIO");
        linha();
        System.out.println("[ EXECUCAO ] - QTD: " + qtd);
        System.out.println("[ EXECUCAO ] - AMBIENTE: " + urlBase);
        System.out.println("[ EXECUCAO ] - AGENCIA: " + agencia);
        System.out.println("[ EXECUCAO ] - CONTA: " + conta);
        linha();

        long mci = 0;
        String cpf = "";
        String token = "";
        String matriculaProjecoes = "";

        try {
            LoginBusiness loginBusiness = new LoginBusiness();
            MciBusiness mciBusiness = new MciBusiness();

            //  --  EFETUAR LOGIN
            //  --  BUSCAR CPF
            //  --  BUSCAR LISTA DE MATRICULAS

            //  --  LOGIN
            LoginResponse loginResponse = loginBusiness.login(urlBase, agencia, conta);

            if(loginResponse != null && loginResponse.getResponse() != null && loginResponse.getResponse().getToken() != null && !AppUtil.stringIsNullOrEmpty(loginResponse.getResponse().getToken().getAccess_token()) && !AppUtil.longIsNullOrZero(loginResponse.getResponse().getDadosCliente().getMci())) {
                //  --  MCI/CPF
                token = loginResponse.getResponse().getToken().getAccess_token();
                mci = loginResponse.getResponse().getDadosCliente().getMci();
                System.out.println("[ EXECUCAO ] - MCI: " + mci);

                MciCpfResponse mciCpfResponse = mciBusiness.buscarCpf(urlBase, loginResponse.getResponse().getToken().getAccess_token(), mci);

                if(mciCpfResponse != null && mciCpfResponse.getResponse() != null && !AppUtil.stringIsNullOrEmpty(mciCpfResponse.getResponse().getCpfCgc())) {
                    cpf = mciCpfResponse.getResponse().getCpfCgc();
                    System.out.println("[ EXECUCAO ] - CPF: " + cpf);
                } else {
                    linha();
                    System.out.println("[ EXECUCAO ] - ERRO DE MCI");
                }
            } else {
                linha();
                System.out.println("[ EXECUCAO ] - ERRO DE LOGIN");
            }

            //  --  DEU CERTO LOGIN E MCI/CPF, SEGUE O BAILE

            //  --  START DE EXECUCOES DE FORMA ASSINCRONA COM TODOS
            //  --  NO CASO DAS PROJECOES, PEGAR PRIMEIRA MATRICULA PARA SIMULAR

            if(!AppUtil.stringIsNullOrEmpty(token) && !AppUtil.stringIsNullOrEmpty(cpf) && !AppUtil.longIsNullOrZero(mci)) {
                if(execProjRenda || execProjReserva) {
                    linha();
                    System.out.println("[ EXECUCAO ] - BUSCA DE MATRICULAS PARA SIMULACAO DAS PROJECOES");
                    MatriculaResponse matriculaResponse = new MatriculaBusiness(urlBase, token, cpf, mci).buscarLista();

                    if (matriculaResponse != null && matriculaResponse.getResponse() != null
                            && matriculaResponse.getResponse().getCdStatusHttp().equals("200")
                            && matriculaResponse.getResponse().getRpstConsultar() != null
                            && matriculaResponse.getResponse().getRpstConsultar().getListaMatricula() != null
                            && !matriculaResponse.getResponse().getRpstConsultar().getListaMatricula().isEmpty()) {
                        matriculaProjecoes = matriculaResponse.getResponse().getRpstConsultar().getListaMatricula().get(0).getNumeroMatricula();
                        System.out.println("[ EXECUCAO ] - BUSCA DE MATRICULAS PARA SIMULACAO DAS PROJECOES FINALIZADA COM SUCESSO - MATRICULA: " + matriculaProjecoes);
                    } else {
                        linha();
                        System.out.println("[ EXECUCAO ] - BUSCA DE MATRICULAS PARA SIMULACAO DAS PROJECOES FINALIZADA COM ERRO");
                    }
                }

                linha();
                System.out.println("[ EXECUCAO ] - MONTANDO LISTA DE REQUISICOES");

                //  --  MONTA A LISTA DE EXECUCOES
                List<ExecucaoRequisitaDTO> listaRequisicoes = new ArrayList<>();

                for(int i = 0; i < qtd; i++) {
                    if(execMatriculas) {
                        ExecucaoRequisitaDTO exec = new ExecucaoRequisitaDTO();
                        exec.setToken(token);
                        exec.setUrlBase(urlBase);
                        exec.setTipo(TipoExecucaoEnum.LISTA_MATRICULAS);
                        exec.setCpf(cpf);
                        exec.setMatricula(matriculaProjecoes);
                        exec.setMci(mci);

                        listaRequisicoes.add(exec);
                    }

                    if(execExtrato) {
                        ExecucaoRequisitaDTO exec = new ExecucaoRequisitaDTO();
                        exec.setToken(token);
                        exec.setUrlBase(urlBase);
                        exec.setTipo(TipoExecucaoEnum.EXTRATO);
                        exec.setCpf(cpf);
                        exec.setMatricula(matriculaProjecoes);
                        exec.setMci(mci);

                        listaRequisicoes.add(exec);
                    }

                    if(execSaldo) {
                        ExecucaoRequisitaDTO exec = new ExecucaoRequisitaDTO();
                        exec.setToken(token);
                        exec.setUrlBase(urlBase);
                        exec.setTipo(TipoExecucaoEnum.SALDO);
                        exec.setCpf(cpf);
                        exec.setMatricula(matriculaProjecoes);
                        exec.setMci(mci);

                        listaRequisicoes.add(exec);
                    }

                    if(execProjRenda) {
                        ExecucaoRequisitaDTO exec = new ExecucaoRequisitaDTO();
                        exec.setToken(token);
                        exec.setUrlBase(urlBase);
                        exec.setTipo(TipoExecucaoEnum.PROJECAO_RENDA);
                        exec.setCpf(cpf);
                        exec.setMatricula(matriculaProjecoes);
                        exec.setMci(mci);

                        listaRequisicoes.add(exec);
                    }

                    if(execProjReserva) {
                        ExecucaoRequisitaDTO exec = new ExecucaoRequisitaDTO();
                        exec.setToken(token);
                        exec.setUrlBase(urlBase);
                        exec.setTipo(TipoExecucaoEnum.PROJECAO_RESERVA);
                        exec.setCpf(cpf);
                        exec.setMatricula(matriculaProjecoes);
                        exec.setMci(mci);

                        listaRequisicoes.add(exec);
                    }
                }

                if(!listaRequisicoes.isEmpty()) {
                    linha();
                    System.out.println("[ EXECUCAO ] - EXECUTANDO REQUISICOES..");

                    List<ExecucaoItemDTO> retornosMatriculas = new Vector<>();
                    List<ExecucaoItemDTO> retornosExtratos = new Vector<>();
                    List<ExecucaoItemDTO> retornosSaldos = new Vector<>();
                    List<ExecucaoItemDTO> retornosProjsRenda = new Vector<>();
                    List<ExecucaoItemDTO> retornosProjsReserva = new Vector<>();

                    listaRequisicoes.parallelStream().forEach(item -> {
                        if(item.getTipo().equals(TipoExecucaoEnum.LISTA_MATRICULAS)) {
                            retornosMatriculas.add(new MatriculaBusiness(item.getUrlBase(), item.getToken(), item.getCpf(), item.getMci()).buscar());
                        } else if(item.getTipo().equals(TipoExecucaoEnum.EXTRATO)) {
                            retornosExtratos.add(new ExtratoBusiness(item.getUrlBase(), item.getToken(), item.getCpf(), item.getMci()).buscar());
                        } else if(item.getTipo().equals(TipoExecucaoEnum.SALDO)) {
                            retornosSaldos.add(new SaldoBusiness(item.getUrlBase(), item.getToken(), item.getCpf(), item.getMci()).buscar());
                        } else if(item.getTipo().equals(TipoExecucaoEnum.PROJECAO_RENDA)) {
                            retornosProjsRenda.add(new ProjecaoRendaBusiness(item.getUrlBase(), item.getToken(), item.getCpf(), item.getMci(), item.getMatricula()).buscar());
                        } else if(item.getTipo().equals(TipoExecucaoEnum.PROJECAO_RESERVA)) {
                            retornosProjsReserva.add(new ProjecaoReservaBusiness(item.getUrlBase(), item.getToken(), item.getCpf(), item.getMci(), item.getMatricula()).buscar());
                        }
                    });

                    linha();
                    System.out.println("[ EXECUCAO ] - RESULTADOS:");

                    //  --  RESULTADOS
                    if(execMatriculas) {
                        List<Long> tempos = new Vector<>();
                        AtomicLong qtdExecucoes = new AtomicLong();
                        AtomicLong qtdSucesso = new AtomicLong();
                        AtomicLong qtdErros = new AtomicLong();

                        retornosMatriculas.forEach(item -> {
                            qtdExecucoes.getAndIncrement();

                            if(item.getTempo() > 0) {
                                tempos.add(item.getTempo());
                            }

                            if(item.isSucesso()) {
                                qtdSucesso.getAndIncrement();
                            } else {
                                qtdErros.getAndIncrement();
                            }
                        });

                        tempos.sort(Comparator.naturalOrder());

                        printaResultados("MATRICULAS", qtdExecucoes.get(), qtdSucesso.get(), qtdErros.get(), tempos.get(0), tempos.get(tempos.size() - 1));
                    }

                    if(execExtrato) {
                        List<Long> tempos = new Vector<>();
                        AtomicLong qtdExecucoes = new AtomicLong();
                        AtomicLong qtdSucesso = new AtomicLong();
                        AtomicLong qtdErros = new AtomicLong();

                        retornosExtratos.forEach(item -> {
                            qtdExecucoes.getAndIncrement();

                            if(item.getTempo() > 0) {
                                tempos.add(item.getTempo());
                            }

                            if(item.isSucesso()) {
                                qtdSucesso.getAndIncrement();
                            } else {
                                qtdErros.getAndIncrement();
                            }
                        });

                        tempos.sort(Comparator.naturalOrder());

                        printaResultados("EXTRATOS", qtdExecucoes.get(), qtdSucesso.get(), qtdErros.get(), tempos.get(0), tempos.get(tempos.size() - 1));
                    }

                    if(execSaldo) {
                        List<Long> tempos = new Vector<>();
                        AtomicLong qtdExecucoes = new AtomicLong();
                        AtomicLong qtdSucesso = new AtomicLong();
                        AtomicLong qtdErros = new AtomicLong();

                        retornosSaldos.forEach(item -> {
                            qtdExecucoes.getAndIncrement();

                            if(item.getTempo() > 0) {
                                tempos.add(item.getTempo());
                            }

                            if(item.isSucesso()) {
                                qtdSucesso.getAndIncrement();
                            } else {
                                qtdErros.getAndIncrement();
                            }
                        });

                        tempos.sort(Comparator.naturalOrder());

                        printaResultados("SALDOS", qtdExecucoes.get(), qtdSucesso.get(), qtdErros.get(), tempos.get(0), tempos.get(tempos.size() - 1));
                    }

                    if(execProjRenda) {
                        List<Long> tempos = new Vector<>();
                        AtomicLong qtdExecucoes = new AtomicLong();
                        AtomicLong qtdSucesso = new AtomicLong();
                        AtomicLong qtdErros = new AtomicLong();

                        retornosProjsRenda.forEach(item -> {
                            qtdExecucoes.getAndIncrement();

                            if(item.getTempo() > 0) {
                                tempos.add(item.getTempo());
                            }

                            if(item.isSucesso()) {
                                qtdSucesso.getAndIncrement();
                            } else {
                                qtdErros.getAndIncrement();
                            }
                        });

                        tempos.sort(Comparator.naturalOrder());

                        printaResultados("PROJECOES DE RENDA", qtdExecucoes.get(), qtdSucesso.get(), qtdErros.get(), tempos.get(0), tempos.get(tempos.size() - 1));
                    }

                    if(execProjReserva) {
                        List<Long> tempos = new Vector<>();
                        AtomicLong qtdExecucoes = new AtomicLong();
                        AtomicLong qtdSucesso = new AtomicLong();
                        AtomicLong qtdErros = new AtomicLong();

                        retornosProjsReserva.forEach(item -> {
                            qtdExecucoes.getAndIncrement();

                            if(item.getTempo() > 0) {
                                tempos.add(item.getTempo());
                            }

                            if(item.isSucesso()) {
                                qtdSucesso.getAndIncrement();
                            } else {
                                qtdErros.getAndIncrement();
                            }
                        });

                        tempos.sort(Comparator.naturalOrder());

                        printaResultados("PROJECOES DE RESERVA", qtdExecucoes.get(), qtdSucesso.get(), qtdErros.get(), tempos.get(0), tempos.get(tempos.size() - 1));
                    }
                } else {
                    linha();
                    System.out.println("[ EXECUCAO ] - FIM SEM ITENS PARA EXECUTAR");
                    linha();
                }
            } else {
                linha();
                System.out.println("[ EXECUCAO ] - FIM COM ERRO DE VALORES VAZIOS");
                linha();
            }
        } catch (Exception e) {
            linha();
            System.out.println("[ EXECUCAO ] - FIM COM EXECEPTION");
            linha();
            e.printStackTrace();
        }
    }

    private void linha() {
        System.out.println();
    }

    private void printaResultados(String nome, long qtdExecucoes, long qtdSucesso, long qtdErros, long menorTempo, long maiorTempo) {
        linha();
        System.out.println("---------------------------------------------------------------------------");
        System.out.println(" " + nome);
        System.out.println("    QTD EXECUCOES: " + qtdExecucoes);
        System.out.println("    QTD SUCESSO: " + qtdSucesso);
        System.out.println("    QTD ERROS: " + qtdErros);
        System.out.println("    MENOR TEMPO (MS): " + menorTempo);
        System.out.println("    MAIOR TEMPO (MS): " + maiorTempo);
    }
}
