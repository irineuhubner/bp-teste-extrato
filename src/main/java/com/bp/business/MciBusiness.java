package com.bp.business;

import com.bp.App;
import com.bp.dto.objetos.mci.MciCpfResponse;
import com.bp.dto.utils.RequestAPIDTO;
import com.bp.dto.utils.RequestAPITipoEnum;
import com.bp.dto.utils.ResponseAPIDTO;
import com.bp.util.AppUtil;
import com.bp.util.ConsumeAPIUtil;
import com.bp.util.ObjetoChaveValor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MciBusiness {
    public MciCpfResponse buscarCpf(String urlBase, String token, long mci) {
        System.out.println();
        System.out.println("-> BUSCA DE CPF [ MCI: " + mci + " ] - INICIOU");
        System.out.println();

        try {
            ConsumeAPIUtil consumeAPIUtil = new ConsumeAPIUtil();
            Gson gson = new GsonBuilder().create();

            List<ObjetoChaveValor<String, String>> listaHeaders = new ArrayList<>();
            listaHeaders.add(new ObjetoChaveValor<>("Authorization", "Bearer " + token));

            RequestAPIDTO requestAPIDTO = new RequestAPIDTO();
            requestAPIDTO.setTipo(RequestAPITipoEnum.GET);
            requestAPIDTO.setUrl(urlBase + "/cfe-mci/api/v1/dadosbasicos/" + mci);
            requestAPIDTO.setCabecalhos(listaHeaders);

            ResponseAPIDTO responseAPIDTO = consumeAPIUtil.executa(requestAPIDTO);

            if(responseAPIDTO != null) {
                if(responseAPIDTO.getStatus().is2xxSuccessful()) {
                    MciCpfResponse mciCpfResponse = gson.fromJson(responseAPIDTO.getObjetoJson(), MciCpfResponse.class);

                    if(mciCpfResponse != null && mciCpfResponse.getResponse() != null && !AppUtil.stringIsNullOrEmpty(mciCpfResponse.getResponse().getCpfCgc())) {
                        return mciCpfResponse;
                    } else {
                        System.out.println("-> BUSCA DE CPF [ MCI: " + mci + " ] - ERRO - OBJETOS VAZIOS");
                    }
                } else {
                    System.out.println("-> BUSCA DE CPF [ MCI: " + mci + " ] - ERRO - RESPONSE NULL");
                    System.out.println();
                    System.out.println("-> BUSCA DE CPF [ MCI: " + mci + " ] - STATUS: " + responseAPIDTO.getStatus());
                    System.out.println("-> BUSCA DE CPF [ MCI: " + mci + " ] - MENSAGEM: " + responseAPIDTO.getMensagem());
                }
            } else {
                System.out.println("-> BUSCA DE CPF [ MCI: " + mci + " ] - INICIO");
            }
        } catch (Exception e) {
            System.out.println("-> BUSCA DE CPF [ MCI: " + mci + " ] - EXCEPTION");
            System.out.println();
            e.printStackTrace();
        }

        System.out.println();
        System.out.println("-> BUSCA DE CPF [ MCI: " + mci + " ] - FINALIZOU");

        return null;
    }
}
