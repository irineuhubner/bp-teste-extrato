package com.bp.dto.utils;

import java.util.ArrayList;
import java.util.List;

public enum TipoExecucaoEnum {
    LISTA_MATRICULAS("LISTA MATRICULAS"),
    EXTRATO("EXTRATO"),
    SALDO("SALDO"),
    PROJECAO_RENDA("PROJECAO RENDA"),
    PROJECAO_RESERVA("PROJECAO RESERVA");

    private String name;

    TipoExecucaoEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static List<TipoExecucaoEnum> getList() {
        List<TipoExecucaoEnum> list = new ArrayList<>();
        for(TipoExecucaoEnum e : values()) {
            list.add(e);
        }
        return list;
    }
}
