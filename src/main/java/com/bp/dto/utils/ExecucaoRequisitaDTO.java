package com.bp.dto.utils;

public class ExecucaoRequisitaDTO {
    private TipoExecucaoEnum tipo;
    private long qtd;
    private String urlBase;
    private String token;
    private String matricula;
    private long mci;
    private String cpf;

    public TipoExecucaoEnum getTipo() {
        return tipo;
    }

    public void setTipo(TipoExecucaoEnum tipo) {
        this.tipo = tipo;
    }

    public long getQtd() {
        return qtd;
    }

    public void setQtd(long qtd) {
        this.qtd = qtd;
    }

    public String getUrlBase() {
        return urlBase;
    }

    public void setUrlBase(String urlBase) {
        this.urlBase = urlBase;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public long getMci() {
        return mci;
    }

    public void setMci(long mci) {
        this.mci = mci;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}
