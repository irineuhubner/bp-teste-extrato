package com.bp.dto.utils;

import org.springframework.http.HttpStatus;

public class ResponseAPIDTO {
    private HttpStatus status;
    private String mensagem;
    private String objetoJson;

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getObjetoJson() {
        return objetoJson;
    }

    public void setObjetoJson(String objetoJson) {
        this.objetoJson = objetoJson;
    }
}
