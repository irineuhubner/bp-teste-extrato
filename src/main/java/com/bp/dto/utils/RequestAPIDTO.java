package com.bp.dto.utils;

import com.bp.util.ObjetoChaveValor;

import java.util.List;

public class RequestAPIDTO {
    private RequestAPITipoEnum tipo;
    private String url;
    private String objetoJson;
    private int timeoutMilissegundo;
    private List<ObjetoChaveValor<String, String>> cabecalhos;
    private List<ObjetoChaveValor<String, String>> parametros;
    private String codificacao;

    public RequestAPITipoEnum getTipo() {
        return tipo;
    }

    public void setTipo(RequestAPITipoEnum tipo) {
        this.tipo = tipo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<ObjetoChaveValor<String, String>> getCabecalhos() {
        return cabecalhos;
    }

    public void setCabecalhos(List<ObjetoChaveValor<String, String>> cabecalhos) {
        this.cabecalhos = cabecalhos;
    }

    public List<ObjetoChaveValor<String, String>> getParametros() {
        return parametros;
    }

    public void setParametros(List<ObjetoChaveValor<String, String>> parametros) {
        this.parametros = parametros;
    }

    public String getCodificacao() {
        return codificacao;
    }

    public void setCodificacao(String codificacao) {
        this.codificacao = codificacao;
    }

    public String getObjetoJson() {
        return objetoJson;
    }

    public void setObjetoJson(String objetoJson) {
        this.objetoJson = objetoJson;
    }

    public int getTimeoutMilissegundo() {
        return timeoutMilissegundo;
    }

    public void setTimeoutMilissegundo(int timeoutMilissegundo) {
        this.timeoutMilissegundo = timeoutMilissegundo;
    }
}
