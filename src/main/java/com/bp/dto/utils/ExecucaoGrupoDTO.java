package com.bp.dto.utils;

public class ExecucaoGrupoDTO {
    private TipoExecucaoEnum tipo;
    private long piorTempo;
    private long melhorTempo;
    private long qtdExecucoes;
    private long qtdSucesso;
    private long qtdErro;
    private String matricula;

    public TipoExecucaoEnum getTipo() {
        return tipo;
    }

    public void setTipo(TipoExecucaoEnum tipo) {
        this.tipo = tipo;
    }

    public long getPiorTempo() {
        return piorTempo;
    }

    public void setPiorTempo(long piorTempo) {
        this.piorTempo = piorTempo;
    }

    public long getMelhorTempo() {
        return melhorTempo;
    }

    public void setMelhorTempo(long melhorTempo) {
        this.melhorTempo = melhorTempo;
    }

    public long getQtdExecucoes() {
        return qtdExecucoes;
    }

    public void setQtdExecucoes(long qtdExecucoes) {
        this.qtdExecucoes = qtdExecucoes;
    }

    public long getQtdSucesso() {
        return qtdSucesso;
    }

    public void setQtdSucesso(long qtdSucesso) {
        this.qtdSucesso = qtdSucesso;
    }

    public long getQtdErro() {
        return qtdErro;
    }

    public void setQtdErro(long qtdErro) {
        this.qtdErro = qtdErro;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
}
