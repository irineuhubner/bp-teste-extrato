package com.bp.dto.utils;

public class ExecucaoItemDTO {
    private String dataInicio;
    private String dataFim;
    private long tempo;
    private boolean sucesso;
    private String mensagem;

    public ExecucaoItemDTO() {
        this.sucesso = false;
        this.tempo = 0;
        this.mensagem = "";
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

    public long getTempo() {
        return tempo;
    }

    public void setTempo(long tempo) {
        this.tempo = tempo;
    }

    public boolean isSucesso() {
        return sucesso;
    }

    public void setSucesso() {
        this.sucesso = true;
    }
    public void setErro() {
        this.sucesso = false;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
