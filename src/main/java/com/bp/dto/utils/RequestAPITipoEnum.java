package com.bp.dto.utils;

import java.util.ArrayList;
import java.util.List;

public enum RequestAPITipoEnum {
    POST("POST"),
    GET("GET"),
    PUT("PUT"),
    DELETE("DELETE");

    private String name;

    RequestAPITipoEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static List<RequestAPITipoEnum> getList() {
        List<RequestAPITipoEnum> list = new ArrayList<>();
        for(RequestAPITipoEnum e : values()) {
            list.add(e);
        }
        return list;
    }
}