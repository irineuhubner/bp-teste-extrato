package com.bp.dto.utils;

public class ExecucaoDTO {
    private String dataInicio;
    private String dataFim;
    private long tempoTotal;
    private long tempoLogin;
    private long tempoMci;

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

    public long getTempoTotal() {
        return tempoTotal;
    }

    public void setTempoTotal(long tempoTotal) {
        this.tempoTotal = tempoTotal;
    }

    public long getTempoLogin() {
        return tempoLogin;
    }

    public void setTempoLogin(long tempoLogin) {
        this.tempoLogin = tempoLogin;
    }

    public long getTempoMci() {
        return tempoMci;
    }

    public void setTempoMci(long tempoMci) {
        this.tempoMci = tempoMci;
    }
}
