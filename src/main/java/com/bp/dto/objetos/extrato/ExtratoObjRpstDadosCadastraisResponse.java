package com.bp.dto.objetos.extrato;

public class ExtratoObjRpstDadosCadastraisResponse {
    private String nome;
    private String apelido;
    private String dataNascimento;
    private String numeroCpfParticipante;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getNumeroCpfParticipante() {
        return numeroCpfParticipante;
    }

    public void setNumeroCpfParticipante(String numeroCpfParticipante) {
        this.numeroCpfParticipante = numeroCpfParticipante;
    }
}