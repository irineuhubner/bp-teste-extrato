package com.bp.dto.objetos.extrato;

import java.io.Serializable;

public class ExtratoObjRpstResponse implements Serializable {
    private String dataAtualizacao;
    private ExtratoObjRpstDadosCadastraisResponse dadosCadastrais;
    private ExtratoObjRpstDadosMatriculaResponse dadosMatricula;
    private ExtratoObjRpstDadosPlanoResponse dadosPlano;

    private ExtratoObjRpstSaldoInvestimentoResponse saldoInvestimento;

    public String getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(String dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public ExtratoObjRpstDadosCadastraisResponse getDadosCadastrais() {
        return dadosCadastrais;
    }

    public void setDadosCadastrais(ExtratoObjRpstDadosCadastraisResponse dadosCadastrais) {
        this.dadosCadastrais = dadosCadastrais;
    }

    public ExtratoObjRpstDadosMatriculaResponse getDadosMatricula() {
        return dadosMatricula;
    }

    public void setDadosMatricula(ExtratoObjRpstDadosMatriculaResponse dadosMatricula) {
        this.dadosMatricula = dadosMatricula;
    }

    public ExtratoObjRpstDadosPlanoResponse getDadosPlano() {
        return dadosPlano;
    }

    public void setDadosPlano(ExtratoObjRpstDadosPlanoResponse dadosPlano) {
        this.dadosPlano = dadosPlano;
    }

    public ExtratoObjRpstSaldoInvestimentoResponse getSaldoInvestimento() {
        return saldoInvestimento;
    }

    public void setSaldoInvestimento(ExtratoObjRpstSaldoInvestimentoResponse saldoInvestimento) {
        this.saldoInvestimento = saldoInvestimento;
    }
}
