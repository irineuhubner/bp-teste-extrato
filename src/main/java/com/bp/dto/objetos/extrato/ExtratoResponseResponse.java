package com.bp.dto.objetos.extrato;

import java.io.Serializable;
import java.util.List;

public class ExtratoResponseResponse implements Serializable {
    private String cdStatusHttp;
    private String execDtInicio;
    private String execDtFim;
    private String execTempo;
    private String execMtDtInicio;
    private String execMtDtFim;
    private String execMtTempo;

    private List<ExtratoObjResponse> extratos;

    public String getCdStatusHttp() {
        return cdStatusHttp;
    }

    public void setCdStatusHttp(String cdStatusHttp) {
        this.cdStatusHttp = cdStatusHttp;
    }

    public String getExecDtInicio() {
        return execDtInicio;
    }

    public void setExecDtInicio(String execDtInicio) {
        this.execDtInicio = execDtInicio;
    }

    public String getExecDtFim() {
        return execDtFim;
    }

    public void setExecDtFim(String execDtFim) {
        this.execDtFim = execDtFim;
    }

    public String getExecTempo() {
        return execTempo;
    }

    public void setExecTempo(String execTempo) {
        this.execTempo = execTempo;
    }

    public String getExecMtDtInicio() {
        return execMtDtInicio;
    }

    public void setExecMtDtInicio(String execMtDtInicio) {
        this.execMtDtInicio = execMtDtInicio;
    }

    public String getExecMtDtFim() {
        return execMtDtFim;
    }

    public void setExecMtDtFim(String execMtDtFim) {
        this.execMtDtFim = execMtDtFim;
    }

    public String getExecMtTempo() {
        return execMtTempo;
    }

    public void setExecMtTempo(String execMtTempo) {
        this.execMtTempo = execMtTempo;
    }

    public List<ExtratoObjResponse> getExtratos() {
        return extratos;
    }

    public void setExtratos(List<ExtratoObjResponse> extratos) {
        this.extratos = extratos;
    }
}
