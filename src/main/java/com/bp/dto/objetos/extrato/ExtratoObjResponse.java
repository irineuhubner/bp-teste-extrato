package com.bp.dto.objetos.extrato;

import java.io.Serializable;

public class ExtratoObjResponse implements Serializable {
    private boolean sucesso;
    private String execDtInicio;
    private String execDtFim;
    private String execTempo;
    private String cdStatusHttp;
    private int qtOcrErro;
    private ExtratoObjRpstResponse rpstConsultar;

    public boolean isSucesso() {
        return sucesso;
    }

    public void setSucesso(boolean sucesso) {
        this.sucesso = sucesso;
    }

    public String getExecDtInicio() {
        return execDtInicio;
    }

    public void setExecDtInicio(String execDtInicio) {
        this.execDtInicio = execDtInicio;
    }

    public String getExecDtFim() {
        return execDtFim;
    }

    public void setExecDtFim(String execDtFim) {
        this.execDtFim = execDtFim;
    }

    public String getExecTempo() {
        return execTempo;
    }

    public void setExecTempo(String execTempo) {
        this.execTempo = execTempo;
    }

    public String getCdStatusHttp() {
        return cdStatusHttp;
    }

    public void setCdStatusHttp(String cdStatusHttp) {
        this.cdStatusHttp = cdStatusHttp;
    }

    public int getQtOcrErro() {
        return qtOcrErro;
    }

    public void setQtOcrErro(int qtOcrErro) {
        this.qtOcrErro = qtOcrErro;
    }

    public ExtratoObjRpstResponse getRpstConsultar() {
        return rpstConsultar;
    }

    public void setRpstConsultar(ExtratoObjRpstResponse rpstConsultar) {
        this.rpstConsultar = rpstConsultar;
    }
}
