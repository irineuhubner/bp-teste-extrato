package com.bp.dto.objetos.extrato;

import java.io.Serializable;
import java.math.BigDecimal;

public class ExtratoObjRpstDadosPlanoResponse implements Serializable {
    private String tipoPlano;
    private String nomePlano;
    private long numeroMatricula;
    private BigDecimal tafMedia;

    public String getTipoPlano() {
        return tipoPlano;
    }

    public void setTipoPlano(String tipoPlano) {
        this.tipoPlano = tipoPlano;
    }

    public String getNomePlano() {
        return nomePlano;
    }

    public void setNomePlano(String nomePlano) {
        this.nomePlano = nomePlano;
    }

    public long getNumeroMatricula() {
        return numeroMatricula;
    }

    public void setNumeroMatricula(long numeroMatricula) {
        this.numeroMatricula = numeroMatricula;
    }

    public BigDecimal getTafMedia() {
        return tafMedia;
    }

    public void setTafMedia(BigDecimal tafMedia) {
        this.tafMedia = tafMedia;
    }
}
