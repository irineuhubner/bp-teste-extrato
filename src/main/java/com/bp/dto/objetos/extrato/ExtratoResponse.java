package com.bp.dto.objetos.extrato;

import java.io.Serializable;

public class ExtratoResponse implements Serializable {
    private ExtratoResponseResponse response;

    public ExtratoResponseResponse getResponse() {
        return response;
    }

    public void setResponse(ExtratoResponseResponse response) {
        this.response = response;
    }
}
