package com.bp.dto.objetos.extrato;

import java.io.Serializable;
import java.math.BigDecimal;

public class ExtratoObjRpstSaldoInvestimentoResponse implements Serializable {
    private BigDecimal totalLancamentosParticipante;
    private BigDecimal totalLancamentosEmpresa;

    public BigDecimal getTotalLancamentosParticipante() {
        return totalLancamentosParticipante;
    }

    public void setTotalLancamentosParticipante(BigDecimal totalLancamentosParticipante) {
        this.totalLancamentosParticipante = totalLancamentosParticipante;
    }

    public BigDecimal getTotalLancamentosEmpresa() {
        return totalLancamentosEmpresa;
    }

    public void setTotalLancamentosEmpresa(BigDecimal totalLancamentosEmpresa) {
        this.totalLancamentosEmpresa = totalLancamentosEmpresa;
    }
}