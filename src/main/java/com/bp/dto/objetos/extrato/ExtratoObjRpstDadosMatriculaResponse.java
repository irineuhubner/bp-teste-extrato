package com.bp.dto.objetos.extrato;

import java.io.Serializable;

public class ExtratoObjRpstDadosMatriculaResponse implements Serializable {
    private String tipoTributacao;
    private String tipoPlano;

    public String getTipoTributacao() {
        return tipoTributacao;
    }

    public void setTipoTributacao(String tipoTributacao) {
        this.tipoTributacao = tipoTributacao;
    }

    public String getTipoPlano() {
        return tipoPlano;
    }

    public void setTipoPlano(String tipoPlano) {
        this.tipoPlano = tipoPlano;
    }
}
