package com.bp.dto.objetos.projecoes;

import java.io.Serializable;

public class ProjecaoReservaResponseResponse implements Serializable {
    private String cdStatusHttp;
    private int qtErro;
    private ProjecaoReservaRpstResponse response;

    public String getCdStatusHttp() {
        return cdStatusHttp;
    }

    public void setCdStatusHttp(String cdStatusHttp) {
        this.cdStatusHttp = cdStatusHttp;
    }

    public int getQtErro() {
        return qtErro;
    }

    public void setQtErro(int qtErro) {
        this.qtErro = qtErro;
    }

    public ProjecaoReservaRpstResponse getResponse() {
        return response;
    }

    public void setResponse(ProjecaoReservaRpstResponse response) {
        this.response = response;
    }
}
