package com.bp.dto.objetos.projecoes;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProjecaoRendaRpstResponse implements Serializable {
    private BigDecimal valorBeneficioProjetadoAtual;
    private BigDecimal valorBeneficioProjetadoSimulado;
    private BigDecimal valorBeneficioProjetadoIdeal;
    private BigDecimal percentualContinuidade;
    private String fatorAtuarialConcessao;
    private BigDecimal valorContribuicaoProjetadoPartic;

    public BigDecimal getValorBeneficioProjetadoAtual() {
        return valorBeneficioProjetadoAtual;
    }

    public void setValorBeneficioProjetadoAtual(BigDecimal valorBeneficioProjetadoAtual) {
        this.valorBeneficioProjetadoAtual = valorBeneficioProjetadoAtual;
    }

    public BigDecimal getValorBeneficioProjetadoSimulado() {
        return valorBeneficioProjetadoSimulado;
    }

    public void setValorBeneficioProjetadoSimulado(BigDecimal valorBeneficioProjetadoSimulado) {
        this.valorBeneficioProjetadoSimulado = valorBeneficioProjetadoSimulado;
    }

    public BigDecimal getValorBeneficioProjetadoIdeal() {
        return valorBeneficioProjetadoIdeal;
    }

    public void setValorBeneficioProjetadoIdeal(BigDecimal valorBeneficioProjetadoIdeal) {
        this.valorBeneficioProjetadoIdeal = valorBeneficioProjetadoIdeal;
    }

    public BigDecimal getPercentualContinuidade() {
        return percentualContinuidade;
    }

    public void setPercentualContinuidade(BigDecimal percentualContinuidade) {
        this.percentualContinuidade = percentualContinuidade;
    }

    public String getFatorAtuarialConcessao() {
        return fatorAtuarialConcessao;
    }

    public void setFatorAtuarialConcessao(String fatorAtuarialConcessao) {
        this.fatorAtuarialConcessao = fatorAtuarialConcessao;
    }

    public BigDecimal getValorContribuicaoProjetadoPartic() {
        return valorContribuicaoProjetadoPartic;
    }

    public void setValorContribuicaoProjetadoPartic(BigDecimal valorContribuicaoProjetadoPartic) {
        this.valorContribuicaoProjetadoPartic = valorContribuicaoProjetadoPartic;
    }
}
