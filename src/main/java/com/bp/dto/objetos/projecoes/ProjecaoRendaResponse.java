package com.bp.dto.objetos.projecoes;

import java.io.Serializable;

public class ProjecaoRendaResponse implements Serializable {
    private ProjecaoRendaResponseResponse response;

    public ProjecaoRendaResponseResponse getResponse() {
        return response;
    }

    public void setResponse(ProjecaoRendaResponseResponse response) {
        this.response = response;
    }
}
