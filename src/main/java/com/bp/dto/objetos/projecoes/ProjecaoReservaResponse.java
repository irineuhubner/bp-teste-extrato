package com.bp.dto.objetos.projecoes;

import java.io.Serializable;

public class ProjecaoReservaResponse implements Serializable {
    private ProjecaoReservaResponseResponse response;

    public ProjecaoReservaResponseResponse getResponse() {
        return response;
    }

    public void setResponse(ProjecaoReservaResponseResponse response) {
        this.response = response;
    }
}
