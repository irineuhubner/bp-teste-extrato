package com.bp.dto.objetos.projecoes;

import java.io.Serializable;

public class ProjecaoRendaResponseResponse implements Serializable {
    private String cdStatusHttp;
    private int qtdOcrErro;
    private ProjecaoRendaRpstResponse rpstConsultar;

    public String getCdStatusHttp() {
        return cdStatusHttp;
    }

    public void setCdStatusHttp(String cdStatusHttp) {
        this.cdStatusHttp = cdStatusHttp;
    }

    public int getQtdOcrErro() {
        return qtdOcrErro;
    }

    public void setQtdOcrErro(int qtdOcrErro) {
        this.qtdOcrErro = qtdOcrErro;
    }

    public ProjecaoRendaRpstResponse getRpstConsultar() {
        return rpstConsultar;
    }

    public void setRpstConsultar(ProjecaoRendaRpstResponse rpstConsultar) {
        this.rpstConsultar = rpstConsultar;
    }
}
