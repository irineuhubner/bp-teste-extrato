package com.bp.dto.objetos.projecoes;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProjecaoReservaRpstResponse implements Serializable {
    private BigDecimal valorReservaProjetadaParticipante;
    private BigDecimal valorRendReservaProjetadaPartic;
    private BigDecimal valorContribReservaProjecaoPartic;
    private BigDecimal valorContribProjetadoParticipante;

    public BigDecimal getValorReservaProjetadaParticipante() {
        return valorReservaProjetadaParticipante;
    }

    public void setValorReservaProjetadaParticipante(BigDecimal valorReservaProjetadaParticipante) {
        this.valorReservaProjetadaParticipante = valorReservaProjetadaParticipante;
    }

    public BigDecimal getValorRendReservaProjetadaPartic() {
        return valorRendReservaProjetadaPartic;
    }

    public void setValorRendReservaProjetadaPartic(BigDecimal valorRendReservaProjetadaPartic) {
        this.valorRendReservaProjetadaPartic = valorRendReservaProjetadaPartic;
    }

    public BigDecimal getValorContribReservaProjecaoPartic() {
        return valorContribReservaProjecaoPartic;
    }

    public void setValorContribReservaProjecaoPartic(BigDecimal valorContribReservaProjecaoPartic) {
        this.valorContribReservaProjecaoPartic = valorContribReservaProjecaoPartic;
    }

    public BigDecimal getValorContribProjetadoParticipante() {
        return valorContribProjetadoParticipante;
    }

    public void setValorContribProjetadoParticipante(BigDecimal valorContribProjetadoParticipante) {
        this.valorContribProjetadoParticipante = valorContribProjetadoParticipante;
    }
}
