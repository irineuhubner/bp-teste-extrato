package com.bp.dto.objetos.saldo;

import java.io.Serializable;

public class SaldoResponse implements Serializable {
    private SaldoResponseResponse response;

    public SaldoResponseResponse getResponse() {
        return response;
    }

    public void setResponse(SaldoResponseResponse response) {
        this.response = response;
    }
}
