package com.bp.dto.objetos.saldo;

import java.io.Serializable;
import java.math.BigDecimal;

public class SaldoResponseResponse implements Serializable {
    private BigDecimal saldoInvestimento;
    private BigDecimal saldoRendimentoInvestimento;

    public BigDecimal getSaldoInvestimento() {
        return saldoInvestimento;
    }

    public void setSaldoInvestimento(BigDecimal saldoInvestimento) {
        this.saldoInvestimento = saldoInvestimento;
    }

    public BigDecimal getSaldoRendimentoInvestimento() {
        return saldoRendimentoInvestimento;
    }

    public void setSaldoRendimentoInvestimento(BigDecimal saldoRendimentoInvestimento) {
        this.saldoRendimentoInvestimento = saldoRendimentoInvestimento;
    }
}
