package com.bp.dto.objetos.mci;

import java.io.Serializable;

public class MciCpfResponse implements Serializable {
    private MciCpfResponseResponse response;

    public MciCpfResponseResponse getResponse() {
        return response;
    }

    public void setResponse(MciCpfResponseResponse response) {
        this.response = response;
    }
}
