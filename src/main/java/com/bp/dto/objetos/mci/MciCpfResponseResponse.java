package com.bp.dto.objetos.mci;

import java.io.Serializable;

public class MciCpfResponseResponse implements Serializable {
    private int tipo;
    private String cpfCgc;

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getCpfCgc() {
        return cpfCgc;
    }

    public void setCpfCgc(String cpfCgc) {
        this.cpfCgc = cpfCgc;
    }
}
