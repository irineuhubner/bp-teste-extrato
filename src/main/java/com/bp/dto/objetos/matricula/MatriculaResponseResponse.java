package com.bp.dto.objetos.matricula;

import java.io.Serializable;

public class MatriculaResponseResponse implements Serializable {
    private String cdStatusHttp;
    private String execDtInicio;
    private String execDtFim;
    private String execTempo;
    private int qtOcrErro;

    private MatriculaRpstResponse rpstConsultar;

    public String getCdStatusHttp() {
        return cdStatusHttp;
    }

    public void setCdStatusHttp(String cdStatusHttp) {
        this.cdStatusHttp = cdStatusHttp;
    }

    public String getExecDtInicio() {
        return execDtInicio;
    }

    public void setExecDtInicio(String execDtInicio) {
        this.execDtInicio = execDtInicio;
    }

    public String getExecDtFim() {
        return execDtFim;
    }

    public void setExecDtFim(String execDtFim) {
        this.execDtFim = execDtFim;
    }

    public String getExecTempo() {
        return execTempo;
    }

    public void setExecTempo(String execTempo) {
        this.execTempo = execTempo;
    }

    public int getQtOcrErro() {
        return qtOcrErro;
    }

    public void setQtOcrErro(int qtOcrErro) {
        this.qtOcrErro = qtOcrErro;
    }

    public MatriculaRpstResponse getRpstConsultar() {
        return rpstConsultar;
    }

    public void setRpstConsultar(MatriculaRpstResponse rpstConsultar) {
        this.rpstConsultar = rpstConsultar;
    }
}
