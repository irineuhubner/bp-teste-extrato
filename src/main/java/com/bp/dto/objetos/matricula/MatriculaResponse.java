package com.bp.dto.objetos.matricula;

import java.io.Serializable;

public class MatriculaResponse implements Serializable {
    private MatriculaResponseResponse response;

    public MatriculaResponseResponse getResponse() {
        return response;
    }

    public void setResponse(MatriculaResponseResponse response) {
        this.response = response;
    }
}
