package com.bp.dto.objetos.matricula;

import java.io.Serializable;
import java.util.List;

public class MatriculaRpstResponse implements Serializable {
    private int qtOcrMatricula;

    private List<MatriculaRpstItemResponse> listaMatricula;

    public int getQtOcrMatricula() {
        return qtOcrMatricula;
    }

    public void setQtOcrMatricula(int qtOcrMatricula) {
        this.qtOcrMatricula = qtOcrMatricula;
    }

    public List<MatriculaRpstItemResponse> getListaMatricula() {
        return listaMatricula;
    }

    public void setListaMatricula(List<MatriculaRpstItemResponse> listaMatricula) {
        this.listaMatricula = listaMatricula;
    }
}
