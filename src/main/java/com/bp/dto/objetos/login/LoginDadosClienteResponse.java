package com.bp.dto.objetos.login;

import java.io.Serializable;

public class LoginDadosClienteResponse implements Serializable {
    private long mci;
    private long agencia;
    private long tipoConta;
    private long titularidade;
    private String nomeCliente;
    private int clienteExterior;
    private long conta;
    private boolean funcionarioBB;

    public long getMci() {
        return mci;
    }

    public void setMci(long mci) {
        this.mci = mci;
    }

    public long getAgencia() {
        return agencia;
    }

    public void setAgencia(long agencia) {
        this.agencia = agencia;
    }

    public long getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(long tipoConta) {
        this.tipoConta = tipoConta;
    }

    public long getTitularidade() {
        return titularidade;
    }

    public void setTitularidade(long titularidade) {
        this.titularidade = titularidade;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public int getClienteExterior() {
        return clienteExterior;
    }

    public void setClienteExterior(int clienteExterior) {
        this.clienteExterior = clienteExterior;
    }

    public long getConta() {
        return conta;
    }

    public void setConta(long conta) {
        this.conta = conta;
    }

    public boolean isFuncionarioBB() {
        return funcionarioBB;
    }

    public void setFuncionarioBB(boolean funcionarioBB) {
        this.funcionarioBB = funcionarioBB;
    }
}
