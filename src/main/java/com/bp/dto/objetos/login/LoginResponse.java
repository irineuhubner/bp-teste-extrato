package com.bp.dto.objetos.login;

import com.bp.dto.objetos.login.LoginResponseResponse;

import java.io.Serializable;

public class LoginResponse implements Serializable {
    private LoginResponseResponse response;

    public LoginResponseResponse getResponse() {
        return response;
    }

    public void setResponse(LoginResponseResponse response) {
        this.response = response;
    }
}
