package com.bp.dto.objetos.login;

import java.io.Serializable;

public class LoginRequest implements Serializable {
    private long agencia;
    private long conta;
    private long titularidade;
    private String senha;

    public long getAgencia() {
        return agencia;
    }

    public void setAgencia(long agencia) {
        this.agencia = agencia;
    }

    public long getConta() {
        return conta;
    }

    public void setConta(long conta) {
        this.conta = conta;
    }

    public long getTitularidade() {
        return titularidade;
    }

    public void setTitularidade(long titularidade) {
        this.titularidade = titularidade;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
