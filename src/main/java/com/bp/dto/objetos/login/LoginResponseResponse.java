package com.bp.dto.objetos.login;

import java.io.Serializable;

public class LoginResponseResponse implements Serializable {
    private LoginTokenResponse token;
    private LoginDadosClienteResponse dadosCliente;

    public LoginTokenResponse getToken() {
        return token;
    }

    public void setToken(LoginTokenResponse token) {
        this.token = token;
    }

    public LoginDadosClienteResponse getDadosCliente() {
        return dadosCliente;
    }

    public void setDadosCliente(LoginDadosClienteResponse dadosCliente) {
        this.dadosCliente = dadosCliente;
    }
}
