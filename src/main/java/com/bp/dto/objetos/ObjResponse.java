package com.bp.dto.objetos;

import java.io.Serializable;

public class ObjResponse<T> implements Serializable {
    private String cdStatusHttp;
    private int qtErro;
    private T rpstConsultar;

    public String getCdStatusHttp() {
        return cdStatusHttp;
    }

    public void setCdStatusHttp(String cdStatusHttp) {
        this.cdStatusHttp = cdStatusHttp;
    }

    public int getQtErro() {
        return qtErro;
    }

    public void setQtErro(int qtErro) {
        this.qtErro = qtErro;
    }

    public T getRpstConsultar() {
        return rpstConsultar;
    }

    public void setRpstConsultar(T rpstConsultar) {
        this.rpstConsultar = rpstConsultar;
    }
}
