package com.bp.util;

public class ObjetoChaveValor<A, B> {
    private A chave;
    private B valor;

    public ObjetoChaveValor(A chave, B valor) {
        this.chave = chave;
        this.valor = valor;
    }

    public A getChave() {
        return chave;
    }

    public void setChave(A chave) {
        this.chave = chave;
    }

    public B getValor() {
        return valor;
    }

    public void setValor(B valor) {
        this.valor = valor;
    }
}