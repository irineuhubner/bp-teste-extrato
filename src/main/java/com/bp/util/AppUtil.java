package com.bp.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

public class AppUtil {
    public static boolean objectIsNull(Object obj) {
        if(obj != null) {
            return true;
        }

        return false;
    }

    public static boolean integerIsNullOrZero(Integer obj) {
        if(obj != null && obj > 0) {
            return false;
        }

        return true;
    }

    public static boolean longIsNullOrZero(Long obj) {
        if(obj != null && obj > 0) {
            return false;
        }

        return true;
    }

    public static boolean stringIsNullOrEmpty(String obj) {
        if(obj != null && obj.length() > 0) {
            return false;
        }

        return true;
    }

    public static Integer getRandonNumber() {
        return ThreadLocalRandom.current().nextInt(100000, 999995);
    }

    public static boolean validateConvertStringToLong(String value) {
        try {
            Long.parseLong(value);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String datetimeNowMiliSecondsStringENG() {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
            return formatter.format(new Date());
        } catch (Exception e) {
            return "";
        }
    }

    public static long tempoExecucao(String dataInicio, String dataFim) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

            Date d1 = format.parse(!AppUtil.stringIsNullOrEmpty(dataInicio) ? dataInicio : datetimeNowMiliSecondsStringENG());
            Date d2 = format.parse(dataFim);

            if(d1 != null && d2 != null) {
                return d2.getTime() - d1.getTime();
            }

            return -1;
        } catch (Exception e) {
            return -2;
        }
    }
}
