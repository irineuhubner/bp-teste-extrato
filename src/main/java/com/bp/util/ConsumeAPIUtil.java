package com.bp.util;

import com.bp.dto.utils.RequestAPIDTO;
import com.bp.dto.utils.ResponseAPIDTO;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

@Component
public class ConsumeAPIUtil {
    public ResponseAPIDTO executa(RequestAPIDTO request) {
        ResponseAPIDTO response = new ResponseAPIDTO();

        try {
            URL url = new URL(request.getUrl());

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod(request.getTipo().getName());
            connection.setDoOutput(true);
            connection.setConnectTimeout(!AppUtil.integerIsNullOrZero(request.getTimeoutMilissegundo()) ? request.getTimeoutMilissegundo() : 180000);
            connection.setReadTimeout(!AppUtil.integerIsNullOrZero(request.getTimeoutMilissegundo()) ? request.getTimeoutMilissegundo() : 180000);

            connection.setRequestProperty("Accept", "application/json");

            if(!AppUtil.stringIsNullOrEmpty(request.getCodificacao())) {
                connection.setRequestProperty("Content-Type", "application/json; charset=" + request.getCodificacao());
            } else {
                connection.setRequestProperty("Content-Type", "application/json");
            }

            if(request.getCabecalhos() != null && !request.getCabecalhos().isEmpty()) {
                for(ObjetoChaveValor<String, String> item : request.getCabecalhos()) {
                    connection.setRequestProperty(item.getChave(), item.getValor());
                }
            }

            if(!AppUtil.stringIsNullOrEmpty(request.getObjetoJson())) {
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
                writer.write(request.getObjetoJson());
                writer.close();
            }

            response.setStatus(HttpStatus.valueOf(connection.getResponseCode()));
            response.setMensagem(connection.getResponseMessage());

            StringBuffer contentBufferJson = new StringBuffer();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            if(br != null) {
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    contentBufferJson.append(inputLine);
                }

                br.close();
            }

            connection.disconnect();

            response.setObjetoJson(contentBufferJson.toString());
        } catch (Exception e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            response.setMensagem(e.toString());
        }

        return response;
    }
}
