package com.bp;

import com.bp.business.RunBusiness;
import com.bp.util.AppUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Scanner;

public class App {
    public static void main( String[] args ) throws IOException {
        linha();
        System.out.println("INICIANDO PROCESSO DE TESTES");

        try {
            boolean exec = true;

            Scanner scn = new Scanner(System.in);

            while (exec) {
                boolean opcaoCorreta = false;

                opcoes();
                int opcaoMenu = scn.nextInt();

                //  String urlBase, long agencia, long conta, long qtd, String email, boolean execMatriculas, boolean execExtrato, boolean execSaldo, boolean execProjRenda, boolean execProjReserva

                boolean executaMatriculas = false;
                boolean executaExtrato = false;
                boolean executaSaldo = false;
                boolean executaProjRenda = false;
                boolean executaProjReserva = false;


                if (opcaoMenu == 1) {
                    executaMatriculas = true;

                    opcaoCorreta = true;
                } else if (opcaoMenu == 2) {
                    executaExtrato = true;

                    opcaoCorreta = true;
                } else if (opcaoMenu == 3) {
                    executaSaldo = true;

                    opcaoCorreta = true;
                } else if (opcaoMenu == 4) {
                    executaProjRenda = true;

                    opcaoCorreta = true;
                } else if (opcaoMenu == 5) {
                    executaProjReserva = true;

                    opcaoCorreta = true;
                } else if (opcaoMenu == 6) {
                    executaMatriculas = true;
                    executaExtrato = true;
                    executaSaldo = true;
                    executaProjRenda = true;
                    executaProjReserva = true;

                    opcaoCorreta = true;
                }

                linha();
                opcoesAmbientes();
                int opcaoAmbiente = scn.nextInt();

                linha();
                System.out.println("AGENCIA (SEM DIGITO):");
                long agencia = scn.nextLong();

                linha();
                System.out.println("CONTA (SEM DIGITO):");
                long conta = scn.nextLong();

                linha();
                System.out.println("QUANTIDADE DE EXECUCOES:");
                long qtd = scn.nextLong();

                String ambiente = "";
                if(opcaoAmbiente == 1) {
                    ambiente = "https://mob.hm.bb.com.br";
                } else if(opcaoAmbiente == 2) {
                    ambiente = "https://mobileteste.hm.bb.com.br";
                } else if(opcaoAmbiente == 3) {
                    ambiente = "https://mobileprojetos.hm.bb.com.br";
                }

                if(opcaoCorreta && !AppUtil.stringIsNullOrEmpty(ambiente) && !AppUtil.longIsNullOrZero(agencia) && !AppUtil.longIsNullOrZero(conta) && !AppUtil.longIsNullOrZero(qtd)) {
                    new RunBusiness().executa(ambiente, agencia, conta, qtd, null, executaMatriculas, executaExtrato, executaSaldo, executaProjRenda, executaProjReserva);
                } else {
                    linha();
                    System.out.println("OPCOES SELECIONADAS/INFORMADAS INCORRETAS");
                }

                linha();
                System.out.println("DESEJA TENTAR NOVAMENTE? [ S ] ou [ N ]");
                String opcaoSeguir = scn.next();

                if (opcaoSeguir.toUpperCase().equals("N")) {
                    exec = false;
                }
            }

            linha();
            System.out.println("FINALIZOU PROCESSO DE TESTES");
        } catch (Exception e) {
            linha();
            System.out.println("FINALIZOU PROCESSO DE TESTES COM ERRO");
            linha();

            e.printStackTrace();
        }
    }

    private static void linha() {
        System.out.println();
    }

    private static void opcoesAmbientes() {
        linha();
        System.out.println("ESCOLHA UM AMBIENTE:");
        linha();
        System.out.println("  [ 1 ] - https://mob.hm.bb.com.br");
        System.out.println("  [ 2 ] - https://mobileteste.hm.bb.com.br");
        System.out.println("  [ 3 ] - https://mobileprojetos.hm.bb.com.br");
    }

    private static void opcoes() {
        linha();
        System.out.println("ESCOLHA UMA OPCAO:");
        linha();
        System.out.println("  [ 1 ] - LISTAR MATRICULAS");
        System.out.println("  [ 2 ] - EXTRATO");
        System.out.println("  [ 3 ] - SALDO CONSOLIDADO");
        System.out.println("  [ 4 ] - PROJECAO RENDA");
        System.out.println("  [ 5 ] - PROJECAO RESERVA");
        System.out.println("  [ 6 ] - TODOS");
    }
}